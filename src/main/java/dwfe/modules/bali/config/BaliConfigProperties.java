package dwfe.modules.bali.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Validated
@Configuration
@ConfigurationProperties(prefix = "dwfe.bali")
public class BaliConfigProperties implements InitializingBean
{
  private final static Logger log = LoggerFactory.getLogger(BaliConfigProperties.class);

  private String api;

  @Override
  public void afterPropertiesSet() throws Exception
  {
    log.info(toString());
  }

  public String getApi()
  {
    return api;
  }

  public void setApi(String api)
  {
    this.api = api;
  }

  @Override
  public String toString()
  {
    return String.format("%n%n" +
                    "-====================================================-%n" +
                    "|                         Bali                       |%n" +
                    "|----------------------------------------------------|%n" +
                    "|                                                     %n" +
                    "| API version                       %s%n" +
                    "|_____________________________________________________%n%n",
            api
    );
  }
}
