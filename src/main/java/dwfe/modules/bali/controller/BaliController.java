package dwfe.modules.bali.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(path = "#{baliConfigProperties.api}", produces = "application/json; charset=utf-8")
public class BaliController
{
  @GetMapping("/me")
  @PreAuthorize("hasAuthority('BALI_USER')")
  public String me(Principal principal)
  {
    return String.format("id: %s%n%n", principal.getName());
  }

  @GetMapping("/i")
  public String i()
  {
    return String.format("my name is i%n%n");
  }
}
