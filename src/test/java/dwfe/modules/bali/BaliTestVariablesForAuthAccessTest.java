package dwfe.modules.bali;

import dwfe.config.DwfeConfigProperties;
import dwfe.test.config.DwfeTestLevelAuthority;
import dwfe.test.config.DwfeTestVariablesForAuthAccessTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Component
public class BaliTestVariablesForAuthAccessTest extends DwfeTestVariablesForAuthAccessTest
{
  @Autowired
  private DwfeConfigProperties propDwfe;

  @Override
  public Map<String, Map<DwfeTestLevelAuthority, Map<RequestMethod, Map<String, Object>>>> RESOURCE_AUTHORITY_reqDATA()
  {
    return Map.of();
  }
}
