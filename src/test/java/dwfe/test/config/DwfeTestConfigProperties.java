package dwfe.test.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Validated
@Configuration
@ConfigurationProperties(prefix = "dwfe.test")
public class DwfeTestConfigProperties implements InitializingBean
{
  private final static Logger log = LoggerFactory.getLogger(DwfeTestConfigProperties.class);

  private Resource resource = new Resource();

  private OAuth2ClientUntrusted oauth2ClientUntrusted = new OAuth2ClientUntrusted();
  private OAuth2ClientTrusted oauth2ClientTrusted = new OAuth2ClientTrusted();
  private OAuth2ClientUnlimited oAuth2ClientUnlimited = new OAuth2ClientUnlimited();

  private LevelAdmin levelAdmin = new LevelAdmin();
  private LevelUser levelUser = new LevelUser();

  @Override
  public void afterPropertiesSet() throws Exception
  {
    log.info(toString());
  }

  public static class Resource
  {
    // Auth
    private String signIn = "/sign-in";
    private String refreshToken = "/sign-in";
    private String signOut = "/sign-out";

    public String getSignIn()
    {
      return signIn;
    }

    public void setSignIn(String signIn)
    {
      this.signIn = signIn;
    }

    public String getRefreshToken()
    {
      return refreshToken;
    }

    public void setRefreshToken(String refreshToken)
    {
      this.refreshToken = refreshToken;
    }

    public String getSignOut()
    {
      return signOut;
    }

    public void setSignOut(String signOut)
    {
      this.signOut = signOut;
    }
  }

  public static class OAuth2ClientUntrusted
  {
    @NotBlank
    private String id = "untrusted";
    @NotBlank
    private String password = "untrusted";

    @NotNull
    @PositiveOrZero
    private Integer accessTokenValiditySeconds = 60 * 3; // 3 min.
    @NotNull
    @PositiveOrZero
    private Integer refreshTokenValiditySeconds = 1;     // 1 sec.

    public String getId()
    {
      return id;
    }

    public void setId(String id)
    {
      this.id = id;
    }

    public String getPassword()
    {
      return password;
    }

    public void setPassword(String password)
    {
      this.password = password;
    }

    public Integer getAccessTokenValiditySeconds()
    {
      return accessTokenValiditySeconds;
    }

    public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds)
    {
      this.accessTokenValiditySeconds = accessTokenValiditySeconds;
    }

    public Integer getRefreshTokenValiditySeconds()
    {
      return refreshTokenValiditySeconds;
    }

    public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds)
    {
      this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
    }
  }

  public static class OAuth2ClientTrusted
  {
    @NotBlank
    private String id = "trusted";
    @NotBlank
    private String password = "trusted";

    @NotNull
    @PositiveOrZero
    private Integer accessTokenValiditySeconds = 60 * 60 * 24 * 20;   // 20 days
    @NotNull
    @PositiveOrZero
    private Integer refreshTokenValiditySeconds = 60 * 60 * 24 * 340; // 340 days

    public String getId()
    {
      return id;
    }

    public void setId(String id)
    {
      this.id = id;
    }

    public String getPassword()
    {
      return password;
    }

    public void setPassword(String password)
    {
      this.password = password;
    }

    public Integer getAccessTokenValiditySeconds()
    {
      return accessTokenValiditySeconds;
    }

    public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds)
    {
      this.accessTokenValiditySeconds = accessTokenValiditySeconds;
    }

    public Integer getRefreshTokenValiditySeconds()
    {
      return refreshTokenValiditySeconds;
    }

    public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds)
    {
      this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
    }
  }

  public static class OAuth2ClientUnlimited
  {
    @NotBlank
    private String id = "unlimited";
    @NotBlank
    private String password = "unlimited";

    @NotNull
    @PositiveOrZero
    private Integer accessTokenValiditySeconds = 0;  // unlimite

    @NotNull
    @PositiveOrZero
    private Integer refreshTokenValiditySeconds = 1; // 1 sec.

    public String getId()
    {
      return id;
    }

    public void setId(String id)
    {
      this.id = id;
    }

    public String getPassword()
    {
      return password;
    }

    public void setPassword(String password)
    {
      this.password = password;
    }

    public Integer getAccessTokenValiditySeconds()
    {
      return accessTokenValiditySeconds;
    }

    public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds)
    {
      this.accessTokenValiditySeconds = accessTokenValiditySeconds;
    }

    public Integer getRefreshTokenValiditySeconds()
    {
      return refreshTokenValiditySeconds;
    }

    public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds)
    {
      this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
    }
  }

  public static class LevelAdmin
  {
    private String username = "test1";
    private String password = "test11";

    public String getUsername()
    {
      return username;
    }

    public void setUsername(String username)
    {
      this.username = username;
    }

    public String getPassword()
    {
      return password;
    }

    public void setPassword(String password)
    {
      this.password = password;
    }
  }

  public static class LevelUser
  {
    private String username = "test2";
    private String password = "test22";

    public String getUsername()
    {
      return username;
    }

    public void setUsername(String username)
    {
      this.username = username;
    }

    public String getPassword()
    {
      return password;
    }

    public void setPassword(String password)
    {
      this.password = password;
    }
  }

  public Resource getResource()
  {
    return resource;
  }

  public void setResource(Resource resource)
  {
    this.resource = resource;
  }

  public OAuth2ClientUntrusted getOauth2ClientUntrusted()
  {
    return oauth2ClientUntrusted;
  }

  public void setOauth2ClientUntrusted(OAuth2ClientUntrusted oauth2ClientUntrusted)
  {
    this.oauth2ClientUntrusted = oauth2ClientUntrusted;
  }

  public OAuth2ClientTrusted getOauth2ClientTrusted()
  {
    return oauth2ClientTrusted;
  }

  public void setOauth2ClientTrusted(OAuth2ClientTrusted oauth2ClientTrusted)
  {
    this.oauth2ClientTrusted = oauth2ClientTrusted;
  }

  public OAuth2ClientUnlimited getoAuth2ClientUnlimited()
  {
    return oAuth2ClientUnlimited;
  }

  public void setoAuth2ClientUnlimited(OAuth2ClientUnlimited oAuth2ClientUnlimited)
  {
    this.oAuth2ClientUnlimited = oAuth2ClientUnlimited;
  }

  public LevelAdmin getLevelAdmin()
  {
    return levelAdmin;
  }

  public void setLevelAdmin(LevelAdmin levelAdmin)
  {
    this.levelAdmin = levelAdmin;
  }

  public LevelUser getLevelUser()
  {
    return levelUser;
  }

  public void setLevelUser(LevelUser levelUser)
  {
    this.levelUser = levelUser;
  }

  @Override
  public String toString()
  {
    return String.format("%n%n" +
                    "-====================================================-%n" +
                    "|                       DWFE Test                    |%n" +
                    "|----------------------------------------------------|%n" +
                    "|                                                     %n" +
                    "| Auth Resouces:                                      %n" +
                    "|                                                     %n" +
                    "|   Sign in             %s%n" +
                    "|   Refresh token       %s%n" +
                    "|   Sign out            %s%n" +
                    "|                                                     %n" +
                    "| OAuth2 Client Credentials:                          %n" +
                    "|                                                     %n" +
                    "|   Untrusted:                                        %n" +
                    "|     username          %s%n" +
                    "|     password          %s%n" +
                    "|   Trusted:                                          %n" +
                    "|     username          %s%n" +
                    "|     password          %s%n" +
                    "|   Unlimited:                                        %n" +
                    "|     username          %s%n" +
                    "|     password          %s%n" +
                    "|                                                     %n" +
                    "| Nevis predefined test users:                        %n" +
                    "|                                                     %n" +
                    "|   level \"ADMIN\":                                  %n" +
                    "|     username          %s%n" +
                    "|     password          %s%n" +
                    "|   level \"USER\":                                   %n" +
                    "|     username          %s%n" +
                    "|     password          %s%n" +
                    "|                                                     %n" +
                    "|_____________________________________________________%n%n",
            resource.signIn,
            resource.refreshToken,
            resource.signOut,

            oauth2ClientUntrusted.id,
            oauth2ClientUntrusted.password,
            oauth2ClientTrusted.id,
            oauth2ClientTrusted.password,
            oAuth2ClientUnlimited.id,
            oAuth2ClientUnlimited.password,

            levelAdmin.username,
            levelAdmin.password,
            levelUser.username,
            levelUser.password
    );
  }
}
