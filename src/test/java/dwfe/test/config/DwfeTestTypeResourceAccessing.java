package dwfe.test.config;

public enum DwfeTestTypeResourceAccessing
{
  USUAL,
  BAD_ACCESS_TOKEN
}
